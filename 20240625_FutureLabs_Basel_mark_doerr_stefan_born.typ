#import "@preview/polylux:0.3.1": *
//#import themes.simple: *
#import themes.university: *

//#set text(font: "Inria Sans")
 //#set text(font: "TeX Gyre Heros")
//#set text(font: "NunitoSans")
//#set text(font: "Nimbus Sans") 
//#show figure.caption: 10pt


#let source(term) = [#text(size: 12pt)[#term]]

//#show: simple-theme.
#show: university-theme.with(
  short-title: [Full integration labautomation / ML],
  short-author: "mark doerr / stefan born",
  short-date: [2024-06-26],
  color-a: rgb("#0074d9"), //#0C6291
  color-b: rgb("#009401"),
  color-c: rgb("#FBFEF9"),
  //aspect-ratio: "4-3"
)

#title-slide( title: "Full integration of ML into an ontology guided automated robotic workflow ", 
              //subtitle: 
              date: "2024-06-26", 
              authors: "mark doerr, stefan born", 
              institution-name: "university greifswald / tu-berlin",
              logo: image("images/uni-greifswald-logo.png", height: 10%)) // add TU-Berlin logo
  

#focus-slide()[
  motivation: why we need full integration ?
]

#matrix-slide(columns: (4fr, 1fr), 
              rows: (1fr, 4fr),
              new-section: [motivation],
              background-color: rgb("#ffffff") )[
                === protein engineering][][
                  #figure(image("images/ATA_substrate_scoring1.png", width: 68%))
                ][
                  #text(size: 8pt)[Source:
                    Calvelage, S. et al.; A Systematic Analysis of the Substrate Scope of (S)- and (R)-Selective Amine Transaminases. Advanced Synthesis & Catalysis 2017, 359 (23), 4235–4243. 
                    https://doi.org/10.1002/adsc.201701079.

                ]
              ]

#matrix-slide(columns: (4fr, 1fr), 
              rows: (1fr, 4fr),
              background-color: rgb("#ffffff") )[
                === machine learning][][
                  #figure(image("images/Transaminases_anie202301660-fig-0001-m.jpg", width: 88%))
                ][ 
                  #text(size: 8pt)[Source:
                      Structure- and Data-Driven Protein Engineering of Transaminases for Improving Activity and Stereoselectivity 
                      Yu-Fei Ao et. al, Angewandte Chemie 2023.  
                      https://doi.org/10.1002/anie.202301660] 
              ]



#focus-slide(
)[
  LARAsuite
]

#slide(title: "what are we aiming at ? ",
       new-section: [LARA])[
       #image("images/requirements_reprod_exp.svg", height: 100%)

       ]

#slide(title: "LARAsuite overview ")[
       #image("images/LARA_buttom_up_workflow_full.svg", height: 100%)
       ]

#slide(title: "LARA connectivity")[
       #image("images/LARA_network.svg", height: 100%)
       ]

#slide(title: "LARA pythonLab")[
       #image("images/pythonLab_code_example.png", height: 75%)
       #align(right)[#source[https://gilab.com/opensourcelab/pythonlab]]
       ]

#slide(title: "LARA orchestrator / scheduler")[
       #image("images/pythonLabScheduler_detailed_concept_LARA.svg")
       ]

#slide(title: "LARA scheduler (Stefan Maak)")[
       #image("images/20220722_milestone_experiment_13_schedule1_cropped.png", height: 100%)
       ]


#slide(title: "labDataReader/SciDat")[

      #v(1em)
      
      == labDataReader  #source[https://gilab.com/opensourcelab/scientificdata/labDataReader]
      
       - generic reader of proprietary data (e.g. HPLC, plate readers)
       - primary output : pandas data frame and _JSON-LD_ (metadata)

      #v(1em)
      
       == SciDat #source[https://gilab.com/opensourcelab/scientificdata/scidat ]

       - packing tabular data / data frames into _parquet_ files, including _JSON-LD_ metadata

       ]

#focus-slide(
)[
  Automated model building

  Protein Prediction Utilities
]


#let handright = symbol("🖙")
#matrix-slide(columns: (4fr, 1fr), 
              rows: (1fr, 4fr),
              background-color: rgb("#ffffff"),
               new-section: [PPU])[
                === Automated Discovery][][
                  #figure(image("images/circle_2.svg", width: 68%))
                ][ #pause
                  #text(size:20pt)[Automation is most advanced
                  for experiment execution and data collection.] 
                  
                
                  #handright The bottleneck is in the theoretical part (analysis, design)
                
                ]
              

#slide(title: "Full integration of models",
new-section:[PPU])[

#set text(size: 35pt)
 
 Full integration would mean to plug models into the workflow
 - which query the data they need
 - and then predict outcomes of virtual experiments
 - or produce an experimental design for a purpose
 - which  is automatically implemented in a
  robotic platform

]

#slide(title: "Protein Prediction Utilities")[
#set text(size: 35pt)

The python package *Protein Prediction Utilities* 

- tries to provide a generic framework for such models (predictive, DoE)
- eventually pluggable in LARA

for protein engineering.

]

#slide(
        title: "The Case of Protein engineering, I",
        new-section: [PPU])[
#v(2em)

#set text(size: 35pt)  
For *directed evolution of enzymes*, the 
loop is closed *without models*:

- mutate
- screen
- select
        ]

    
#slide( title: "The Case of Protein engineering, II")[

#v(2em)

#set text(size: 35pt)

For *rational design of enzymes*
the loop is closed *by human experts*:

- advanced modelling of enzyme function
- leads to hypothesis on specific mutations
- and proposal of experiments

]

#slide( title: "The Case of Protein engineering, III")[
 
 #set text(size: 35pt)
 Closing the loop by *data driven modelling/ machine learning* is gaining
 ground.
#pause 

Most examples are for special cases with special data-preprocessing.
#pause

What do we need for full automation of data driven modelling and experimental design?    
    
        ]



#slide(title: "How models are given")[
#set text(size: 35pt)
Predictive model families  as components of a workflow

- define inputs and outputs in semantic terms
- have automatic training and cross-validation (CV)
- come with hyperparameter ranges
- are themselves semantically annotated
]


#slide(title: "A remark about a tricky point")[
#set text(size: 35pt)
The semantic annotation of the model includes the
*experimental purpose*.
#pause
- finding new enzymes #sym.arrow.r.double.long CV across enzymes
#pause
- finding new substrates #sym.arrow.r.double.long CV across substrates
#pause
The CV procedure is really part of the model in this sense.
]

#slide(title: "How models can be trained")[
#set text(size: 35pt)
#handright 
Such model families allow
- to construct SPARQL queries for the  aggregation of their input data
- automatic hyperparameter optimization 
]


#slide(title: "Aggregation for a model")[

#image("images/aggregation.svg", height:60%)

- names of predictors linked to
  database properties
- JSON describes the requirements of model
- JSON-LD does semantic annotation


]

#slide(title: "How models predict")[
#set text(size: 35pt)

Model predictions ought to be probabilistic.

Let's look at two cases:

- Some models are natively probabilistic.
- For all models cross-validation ensembles provide estimates of predictive distributions.

#handright needed for model selection, DoE

]


#slide(title: "How models can be retrieved")[
#set text(size: 35pt)
- all training and validation results are saved to a database (MongoDB)
- the trained models are saved to a key-value store
- models can be retrieved, reused and reproduced
#handright FAIR models
]

#slide(title: "How models can be constructed")[
#set text(size: 35pt)

To make such models *generic* tools a non-expert way
of building models is needed.

#handright  Factory functions for large classes
of models.

At present a general factory for *transfer learning models* is available.


]

#slide(title: "Transfer Learning and small data")[
#set text(size: 35pt)

Transfer learning is one possible answer to the
*small data problem* .
#pause

- large amounts of known proteins
- few examples for a specific protein (e.g. enzymatic) function

]

#let caution=symbol(
"☡")

#slide(title: "What is Transfer Learning (TL)?")[
#set text(size: 30pt)
- reuse models trained on large datasets of proteins for some task (ESM 2, BERT, UniRep, AlphaFold ) 
- as components of a model for a new specific task

#pause
#handright cheap potentially useful models
#pause

#caution  but you have to be lucky for one task to help  a different task


]



#slide(title: "TL training and prediction")[

#image("images/data_flow.svg", height:80%)

]


#slide(title: "Many TL models")[
#set text(size: 30pt)

There are by now 
- many large protein models
- many possible downstream model architectures

#pause
#handright We'll have to explore this 
large combinatorial space to assess the usefulness of transfer learning models.

This has to be easy, or we won't do it.
]



#slide( title: "acknowledgements"
)[
  #set text(size: 18pt)
  
    - Stefan Maak
    - Uwe Bornscheuer's group (Univ. Greifswald)

      === project partners
      - Peter Neubauer's group (TU Berlin) 
      - Johannes Kabisch's group and associates (Uni Trondheim)
      - Egon Heuson (Uni Lille)
     

      KIWI-biolab team,  NFDI4Cat teams, SiLA team & AnIML team

      #set text(size: 12pt)

      This work was supported by the German Federal Ministry of Education and Research through the Program “International Future Labs for Artificial Intelligence” (Grant number 01DD20002A) as well as the NFDI4Cat grant 

      We are grateful to the Deutsche Forschungsgemeinschaft (DFG, INST 292/118-1 FUGG) and the federal state Mecklenburg-Vorpommern for financing the robotic platform.
]



